/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Giovanni Bandeira
 */
public class Elipse {
    public double SemiEixo1,SemiEixo2,Area,Perimetro;

    public Elipse(double eixo1, double eixo2) {
        this.SemiEixo1 = eixo1/2.;
        this.SemiEixo2 = eixo2/2.;
    }

    public double getArea() {
        Area=Math.PI*SemiEixo1*SemiEixo2;
        return Area;
    }

    public double getPerimetro() {
        Perimetro=Math.PI*(3.*(SemiEixo1*SemiEixo2)-Math.sqrt((3.*SemiEixo1+SemiEixo2)*(SemiEixo1+3.*SemiEixo2)));
        return Perimetro;
    }
    
}
